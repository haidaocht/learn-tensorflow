Place for everything Tensorflow.

How to install Tensorflow?
-------

* Windows - pip install tensorflow

Lessons
-------

* [01 - Lesson](https://nbviewer.jupyter.org/urls/bitbucket.org/hrojas/learn-tensorflow/raw/master/lessons/01%20-%20Lesson.ipynb)
* [02 - Lesson](https://nbviewer.jupyter.org/urls/bitbucket.org/hrojas/learn-tensorflow/raw/master/lessons/02%20-%20Lesson.ipynb)
* [03 - Lesson](https://nbviewer.jupyter.org/urls/bitbucket.org/hrojas/learn-tensorflow/raw/master/lessons/03%20-%20Lesson.ipynb)
* [04 - Lesson](https://nbviewer.jupyter.org/urls/bitbucket.org/hrojas/learn-tensorflow/raw/master/lessons/04%20-%20Lesson.ipynb)
* [05 - Lesson](https://nbviewer.jupyter.org/urls/bitbucket.org/hrojas/learn-tensorflow/raw/master/lessons/05%20-%20Lesson.ipynb)
* [06 - Lesson](https://nbviewer.jupyter.org/urls/bitbucket.org/hrojas/learn-tensorflow/raw/master/lessons/06%20-%20Lesson.ipynb)
* [07 - Lesson](https://nbviewer.jupyter.org/urls/bitbucket.org/hrojas/learn-tensorflow/raw/master/lessons/07%20-%20Lesson.ipynb)
* [08 - Lesson](https://nbviewer.jupyter.org/urls/bitbucket.org/hrojas/learn-tensorflow/raw/master/lessons/08%20-%20Lesson.ipynb)
* [09 - Lesson](https://nbviewer.jupyter.org/urls/bitbucket.org/hrojas/learn-tensorflow/raw/master/lessons/09%20-%20Lesson.ipynb)
* [10 - Lesson](https://nbviewer.jupyter.org/urls/bitbucket.org/hrojas/learn-tensorflow/raw/master/lessons/10%20-%20Lesson.ipynb)
* [11 - Lesson](https://nbviewer.jupyter.org/urls/bitbucket.org/hrojas/learn-tensorflow/raw/master/lessons/11%20-%20Lesson.ipynb)
* [12 - Lesson](https://nbviewer.jupyter.org/urls/bitbucket.org/hrojas/learn-tensorflow/raw/master/lessons/12%20-%20Lesson.ipynb)
* [13 - Lesson](https://nbviewer.jupyter.org/urls/bitbucket.org/hrojas/learn-tensorflow/raw/master/lessons/13%20-%20Lesson.ipynb)